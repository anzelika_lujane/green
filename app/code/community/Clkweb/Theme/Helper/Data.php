<?php
class Clkweb_Theme_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function useEnhancedMenu() {
        return Mage::getStoreConfig('clkweb_greentheme_options/settings/use_enhanced_menu');
    }
    public function useFullWidthSlider() {
        return Mage::getStoreConfig('clkweb_greentheme_options/settings/use_full_width_slider');
    }
    public function useButtonHeaderCart() {
        return Mage::getStoreConfig('clkweb_greentheme_options/settings/use_button_header_cart');
    }
}
