<?php
$installer = $this;
$installer->startSetup();

if (!Mage::getModel('admin/block')->getCollection()->addFieldToFilter('block_name', 'cms/block')->count()) {
    if ($permission = Mage::getModel('admin/block')) {
        $permission->setBlockName('cms/block');
        $permission->setIsAllowed(1);
        $permission->save();
    }
}

if (!Mage::getModel('admin/block')->getCollection()->addFieldToFilter('block_name', 'newsletter/subscribe')->count()) {
    if ($permission = Mage::getModel('admin/block')) {
        $permission->setBlockName('newsletter/subscribe');
        $permission->setIsAllowed(1);
        $permission->save();
    }
}

if (!Mage::getModel('admin/block')->getCollection()->addFieldToFilter('block_name', 'clkweb_theme/brands')->count()) {
    if ($permission = Mage::getModel('admin/block')) {
        $permission->setBlockName('clkweb_theme/brands');
        $permission->setIsAllowed(1);
        $permission->save();
    }
}

$installer->endSetup();