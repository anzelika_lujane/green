<?php
/* @var $installer Clkweb_Theme_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// Add static block - Header Promos
Mage::getModel('cms/block')->load('header-promo')->delete();

$content = <<<EOF
<ul>
<li><span class="fa fa-check">&nbsp;</span>14 dages returret</li>
<li><span class="fa fa-check">&nbsp;</span>Gratis fragt </li>
<li><span class="fa fa-check">&nbsp;</span>1-3 dages levering</li>
</ul>
EOF;

$installer->addStaticBlock('header-promo', 'Header Promo', $content);

$installer->endSetup();
