<?php
$installer = $this;
$installer->startSetup();

// Create new Product attribute
$installer->removeAttribute('catalog_product', 'size_guide_link');
$attribute  = array(
    'type'                      => 'varchar',
    'label'                     => 'Size Guide Link text',
    'input'                     => 'text',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                   => true,
    'visible_on_front'          => '1',
    'used_in_product_listing'   => '1',
    'required'                  => false,
    'user_defined'              => true,
    'default'                   => "",
    'group'                     => "General"
);

$installer->addAttribute('catalog_product', 'size_guide_link', $attribute);

$installer->removeAttribute('catalog_product', 'size_guide');
$attribute  = array(
    'type'                      => 'text',
    'label'                     => 'Size Guide',
    'input'                     => 'textarea',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                   => true,
    'visible_on_front'          => '1',
    'used_in_product_listing'   => '1',
    'required'                  => false,
    'user_defined'              => true,
    'default'                   => "",
    'group'                     => "General",
    'wysiwyg_enabled'           => true
);

$installer->addAttribute('catalog_product', 'size_guide', $attribute);

$installer->endSetup();
