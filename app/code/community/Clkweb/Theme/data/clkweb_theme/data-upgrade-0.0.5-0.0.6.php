<?php
/* @var $installer Clkweb_Theme_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// Add static block for amasty brand slider
Mage::getModel('cms/block')->load('amasty-brand-slider')->delete();

$content = <<<EOF
{{block type="clkweb_theme/brands" attribute_code="brands" columns="3" template="clkweb/brand-slider.phtml"}}
EOF;

$installer->addStaticBlock('amasty-brand-slider', 'Amasty Brand Slider', $content);

//Add static block for Clkweb brand slider
Mage::getModel('cms/block')->load('clkweb-brand-slider')->delete();

$content = <<<EOF
<div class="brand-slider">
<div><img src="{{media url="wysiwyg/clkwebtheme_images/brand-slider/brand1.png"}}" alt="brand" /></div>
<div><img src="{{media url="wysiwyg/clkwebtheme_images/brand-slider/brand2.png"}}" alt="brand" /></div>
<div><img src="{{media url="wysiwyg/clkwebtheme_images/brand-slider/brand3.png"}}" alt="brand" /></div>
<div><img src="{{media url="wysiwyg/clkwebtheme_images/brand-slider/brand4.png"}}" alt="brand" /></div>
<div><img src="{{media url="wysiwyg/clkwebtheme_images/brand-slider/brand5.png"}}" alt="brand" /></div>
<div><img src="{{media url="wysiwyg/clkwebtheme_images/brand-slider/brand6.png"}}" alt="brand" /></div>
<div><img src="{{media url="wysiwyg/clkwebtheme_images/brand-slider/brand7.png"}}" alt="brand" /></div>
<div><img src="{{media url="wysiwyg/clkwebtheme_images/brand-slider/brand8.png"}}" alt="brand" /></div>
</div>
EOF;

$installer->addStaticBlock('clkweb-brand-slider', 'Clkweb Brand Slider', $content);

$installer->endSetup();
