<?php
/* @var $installer Clkweb_Theme_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();


// Add CMS Page - Åbningstider
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-3">
        <action method="setBlockId"><block_id>footer-column-3</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('abningstider')->delete();
$installer->addCmsPage('abningstider', 'Åbningstider', 'Åbningstider', $content, 'two_columns_left', $layout);


// Add CMS Page - Betaling
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-2">
        <action method="setBlockId"><block_id>footer-column-2</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('betaling')->delete();
$installer->addCmsPage('betaling', 'Betaling', 'Betaling', $content, 'two_columns_left', $layout);


// Add CMS Page - Fragt
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-2">
        <action method="setBlockId"><block_id>footer-column-2</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('fragt')->delete();
$installer->addCmsPage('fragt', 'Fragt', 'Fragt', $content, 'two_columns_left', $layout);


// Add CMS Page - Handelsvilkår
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-2">
        <action method="setBlockId"><block_id>footer-column-2</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('handelsvilkar')->delete();
$installer->addCmsPage('handelsvilkar', 'Handelsvilkår', 'Handelsvilkår', $content, 'two_columns_left', $layout);


// Add CMS Page - Her finder du os
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-1">
        <action method="setBlockId"><block_id>footer-column-1</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('her-finder-du-os')->delete();
$installer->addCmsPage('her-finder-du-os', 'Her finder du os', 'Her finder du os', $content, 'two_columns_left', $layout);


// Add CMS Page - Forside
$content = <<<EOF
<div class="slideshow-container">
<ul class="slideshow">
<li class="slide"><div class="page-width"><img src="{{skin url="images/slider/slider-image.jpg"}}" alt="greentheme" /></div></li>
</ul>
</div>
<div class="content-wrapper">
<div class="promo-blocks">{{block type="cms/block" block_id="home-promo-blocks"}}</div>
<div class="homepage-text">{{block type="cms/block" block_id="home-text"}}</div>
</div>
EOF;

$layout = <<<EOF
<reference name="content">
<block type="core/template" template="catalog/product/homepage-titles.phtml" />
<block type="catalog/product_list" template="catalog/product/home.phtml" name="home_product_list_featured">
    <action method="setColumnCount"><count>4</count></action>
    <action method="setCategoryTitle"><value>Featured</value></action>
    <action method="setCategoryId"><value>24</value></action>
    <action method="setProductLimit"><value>4</value></action>
</block>

<block type="catalog/product_list" template="catalog/product/home.phtml" name="home_product_list_new">
    <action method="setColumnCount"><count>4</count></action>
    <action method="setCategoryTitle"><value>Nye produkter</value></action>
    <action method="setCategoryId"><value>22</value></action>
    <action method="setProductLimit"><value>4</value></action>
</block>

<block type="catalog/product_list" template="catalog/product/home.phtml" name="home_product_list_top">
    <action method="setColumnCount"><count>4</count></action>
    <action method="setCategoryTitle"><value>Populære produkter</value></action>
    <action method="setCategoryId"><value>21</value></action>
    <action method="setProductLimit"><value>4</value></action>
</block>
<block type="core/template" template="page/homepage-text.phtml" name="homepage_text_block">
 <block type="cms/block" name="home_text_title" as="home_text_title">
        <action method="setBlockId">
            <blockId>home_text_title</blockId>
        </action>
    </block>
    <block type="cms/block" name="home_text_1" as="home_text_1">
        <action method="setBlockId">
            <blockId>home_text_1</blockId>
        </action>
    </block>
    <block type="cms/block" name="home_text_2" as="home_text_2">
        <action method="setBlockId">
            <blockId>home_text_2</blockId>
        </action>
    </block>
    <block type="cms/block" name="home_text_3" as="home_text_3">
        <action method="setBlockId">
            <blockId>home_text_3</blockId>
        </action>
    </block>
</block>
</reference>
EOF;

Mage::getModel('cms/page')->load('home')->delete();
$installer->addCmsPage('home', 'Forside', '', $content, 'one_column', $layout);


// Add CMS Page - Om os
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-4">
        <action method="setBlockId"><block_id>footer-column-4</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('om-os')->delete();
$installer->addCmsPage('om-os', 'Om os', 'Om os', $content, 'two_columns_left', $layout);


// Add CMS Page - Returnering af varer
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-1">
        <action method="setBlockId"><block_id>footer-column-1</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('returnering-af-varer')->delete();
$installer->addCmsPage('returnering-af-varer', 'Returnering af varer', 'Returnering af varer', $content, 'two_columns_left', $layout);


// Add CMS Page - Sikker handel
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-3">
        <action method="setBlockId"><block_id>footer-column-3</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('sikker-handel')->delete();
$installer->addCmsPage('sikker-handel', 'Sikker handel', 'Sikker handel', $content, 'two_columns_left', $layout);


// Add CMS Page - Sociale medier
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-4">
        <action method="setBlockId"><block_id>footer-column-4</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('sociale-medier')->delete();
$installer->addCmsPage('sociale-medier', 'Sociale medier', 'Sociale medier', $content, 'two_columns_left', $layout);


// Add CMS Page - Spørgsmål og svar
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-3">
        <action method="setBlockId"><block_id>footer-column-3</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('sporgsmal-og-svar')->delete();
$installer->addCmsPage('sporgsmal-og-svar', 'Spørgsmål og svar', 'Spørgsmål og svar', $content, 'two_columns_left', $layout);


// Add CMS Page - Vores mission
$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dignissim rutrum metus vel vulputate. Curabitur condimentum turpis dolor, consequat dapibus ex rhoncus eget. Nulla facilisi. Quisque elit ex, luctus eu felis quis, accumsan egestas sapien. Phasellus libero ipsum, laoreet ut mattis eget, faucibus eu lectus. Curabitur quis efficitur risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at erat sit amet nisl tincidunt rutrum vitae nec purus. Aliquam a velit commodo, finibus magna maximus, mollis augue. Sed sed massa justo. Maecenas varius, arcu ut tincidunt varius, leo sem aliquet metus, id congue sem dui ac nisi.<br /><br />

Morbi lobortis, tortor et venenatis ultrices, odio eros commodo turpis, non lacinia lectus est at velit. Integer varius arcu non ornare tristique. Ut ex est, aliquam eu metus ac, viverra consequat purus. Integer porttitor faucibus quam, sed faucibus libero tincidunt vel. Pellentesque faucibus libero cursus fermentum commodo. Sed lobortis sem ultrices aliquam pharetra. Fusce ut ex iaculis ante suscipit efficitur id vitae enim. Proin tincidunt elit mauris, id accumsan purus aliquet ut. Donec nec turpis id dui eleifend placerat at cursus mauris. In molestie ut augue vitae dignissim. Vivamus ultrices massa et mi lacinia euismod et luctus risus. Donec ornare, ex in scelerisque eleifend, sem turpis ultrices diam, in tristique lacus velit sit amet leo. Mauris faucibus efficitur bibendum. Praesent ac tellus ligula.
<br /><br />
Pellentesque in vestibulum quam. Proin sit amet odio massa. Sed accumsan gravida mi, nec bibendum lacus gravida in. Fusce ac metus non dui tincidunt sollicitudin. Suspendisse dignissim lobortis neque. Sed vel auctor neque. Vivamus aliquet augue ut arcu scelerisque semper.
<br /><br />
Etiam id tellus at arcu tincidunt bibendum vel eu est. Nam id neque arcu. Suspendisse eget metus ac leo hendrerit scelerisque. Donec vel est auctor nunc eleifend suscipit. Nullam vitae ex nec nisi porta imperdiet. Aenean pretium orci arcu, vel efficitur sapien tristique sed. Sed tincidunt fermentum porta. Ut bibendum tellus sapien, ut egestas arcu tristique ut.
<br /><br />
Donec non felis ligula. Vestibulum vitae elit at lectus pellentesque tempus. Duis maximus, elit vitae placerat semper, eros tortor elementum tellus, eget iaculis quam massa eu nisi. Maecenas id imperdiet metus, sed volutpat neque. Cras ac iaculis augue. In rhoncus id urna a malesuada. Nulla fringilla pellentesque risus at dignissim. Sed sodales vehicula sem, id maximus lacus pretium sed. Donec luctus risus eu urna sodales lobortis. Vivamus urna nisi, congue et pretium sit amet, facilisis id odio.
EOF;

$layout = <<<EOF
<reference name="left">
    <block type="cms/block" name="left-menu-4">
        <action method="setBlockId"><block_id>footer-column-4</block_id></action>
    </block>
    <remove name="right.reports.product.viewed.left"/>
    <remove name="catalog.compare.sidebar.left"/>
</reference>
EOF;

Mage::getModel('cms/page')->load('vores-mission')->delete();
$installer->addCmsPage('vores-mission', 'Vores mission', 'Vores mission', $content, 'two_columns_left', $layout);



$installer->endSetup();
