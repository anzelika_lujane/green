<?php
/* @var $installer Clkweb_Theme_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

Mage::register('isSecureArea', 1);

// Create category for homepage product tabs - 1
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$category = Mage::getModel('catalog/category');
$category->setPath('1/2') // set parent to be root category
    ->setName('homepage_tab1')
    ->setUrlKey('homepage_tab1')
    ->setIsActive(1)
    ->setIncludeInMenu(0)
    ->save();

$categoryTab1 = $category->getId();

// Create category for homepage product tabs - 2
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$category = Mage::getModel('catalog/category');
$category->setPath('1/2') // set parent to be root category
->setName('homepage_tab2')
    ->setUrlKey('homepage_tab2')
    ->setIsActive(1)
    ->setIncludeInMenu(0)
    ->save();

$categoryTab2 = $category->getId();

// Create category for homepage product tabs - 3
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$category = Mage::getModel('catalog/category');
$category->setPath('1/2') // set parent to be root category
->setName('homepage_tab3')
    ->setUrlKey('homepage_tab3')
    ->setIsActive(1)
    ->setIncludeInMenu(0)
    ->save();

$categoryTab3 = $category->getId();

$content = <<<EOF
<div class="slideshow-container">
<ul class="slideshow">
<li class="slide"><div class="page-width"><img src="{{skin url="images/slider/slider-image.jpg"}}" alt="greentheme" /></div></li>
</ul>
</div>
<div class="content-wrapper">
<div class="promo-blocks">{{block type="cms/block" block_id="home-promo-blocks"}}</div>
</div>
EOF;

$layout = <<<EOF
<reference name="content">
<block type="core/template" template="catalog/product/homepage-titles.phtml" />
<block type="catalog/product_list" template="catalog/product/home.phtml" name="home_product_list_featured">
    <action method="setColumnCount"><count>4</count></action>
    <action method="setCategoryTitle"><value>Featured</value></action>
    <action method="setCategoryId"><value>{$categoryTab1}</value></action>
    <action method="setProductLimit"><value>4</value></action>
</block>

<block type="catalog/product_list" template="catalog/product/home.phtml" name="home_product_list_new">
    <action method="setColumnCount"><count>4</count></action>
    <action method="setCategoryTitle"><value>Nye produkter</value></action>
    <action method="setCategoryId"><value>{$categoryTab2}</value></action>
    <action method="setProductLimit"><value>4</value></action>
</block>

<block type="catalog/product_list" template="catalog/product/home.phtml" name="home_product_list_top">
    <action method="setColumnCount"><count>4</count></action>
    <action method="setCategoryTitle"><value>Populære produkter</value></action>
    <action method="setCategoryId"><value>{$categoryTab3}</value></action>
    <action method="setProductLimit"><value>4</value></action>
</block>
<block type="core/template" template="page/homepage-text.phtml" name="homepage_text_block">
 <block type="cms/block" name="home_text_title" as="home_text_title">
        <action method="setBlockId">
            <blockId>home_text_title</blockId>
        </action>
    </block>
    <block type="cms/block" name="home_text_1" as="home_text_1">
        <action method="setBlockId">
            <blockId>home_text_1</blockId>
        </action>
    </block>
    <block type="cms/block" name="home_text_2" as="home_text_2">
        <action method="setBlockId">
            <blockId>home_text_2</blockId>
        </action>
    </block>
    <block type="cms/block" name="home_text_3" as="home_text_3">
        <action method="setBlockId">
            <blockId>home_text_3</blockId>
        </action>
    </block>
</block>
</reference>
EOF;

Mage::getModel('cms/page')->load('home')->delete();
$installer->addCmsPage('home', 'Forside', '', $content, 'one_column', $layout);

$installer->endSetup();

