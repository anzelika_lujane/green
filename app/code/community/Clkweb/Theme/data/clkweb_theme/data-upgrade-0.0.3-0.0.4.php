<?php
/* @var $installer Clkweb_Theme_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->setConfigData('design/package/name', 'clkwebgreen');
$installer->setConfigData('design/theme/default', 'green');

$installer->endSetup();