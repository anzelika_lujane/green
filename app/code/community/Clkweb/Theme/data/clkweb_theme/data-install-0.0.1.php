<?php
/* @var $installer Clkweb_Theme_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// Add static block - Footer Column 1
Mage::getModel('cms/block')->load('footer-column-1')->delete();

$content = <<<EOF
<div class="links">
<div class="block-title"><strong><span>Kontakt os</span></strong></div>
<ul>
<li><a title="Kontaktformular" href="{{store direct_url="contacts}}">Kontaktformular</a></li>
<li><a title="Her finder du os " href="{{store direct_url="her-finder-du-os"}}">Her finder du os</a></li>
<li><a title="Returnering af varer" href="{{store direct_url="returnering-af-varer"}}">Returnering af varer</a></li>
</ul>
</div>
EOF;

$installer->addStaticBlock('footer-column-1', 'Footer Column 1', $content);


// Add static block - Footer Column 2
Mage::getModel('cms/block')->load('footer-column-2')->delete();

$content = <<<EOF
<div class="links">
<div class="block-title"><strong><span>Info og vilkår</span></strong></div>
<ul>
<li><a href="{{store direct_url="handelsvilkar"}}">Handelsvilkår</a></li>
<li><a href="{{store direct_url="betaling"}}">Betaling</a></li>
<li><a href="{{store direct_url="fragt"}}">Fragt</a></li>
</ul>
</div>
EOF;

$installer->addStaticBlock('footer-column-2', 'Footer Column 2', $content);


// Add static block - Footer Column 3
Mage::getModel('cms/block')->load('footer-column-3')->delete();

$content = <<<EOF
<div class="links">
<div class="block-title">
<strong><span>Kundeservice</span></strong>
</div>
<ul>
<li><a href="{{store url="abningstider"}}">Åbningstider</a></li>
<li><a href="{{store url="sikker-handel"}}">Sikker handel</a></li>
<li><a href="{{store url="sporgsmal-og-svar"}}">Spørgsmål og svar</a></li>
</ul>
</div>
EOF;

$installer->addStaticBlock('footer-column-3', 'Footer Column 3', $content);


// Add static block - Footer Column 4
Mage::getModel('cms/block')->load('footer-column-4')->delete();

$content = <<<EOF
<div class="links">
<div class="block-title"><strong><span>Om os</span></strong></div>
<ul>
<li class="first"><a title="Min konto" href="{{store direct_url="om-os"}}">Om os</a></li>
<li><a title="Ordrer og returneringer" href="{{store direct_url="vores-mission"}}">Vores mission</a></li>
<li><a title="How To Shop" href="{{store direct_url="sociale-medier"}}">Sociale medier</a></li>
</ul>
</div>
EOF;

$installer->addStaticBlock('footer-column-4', 'Footer Column 4', $content);


// Add static block - Footer Logo Block
Mage::getModel('cms/block')->load('footer-logo')->delete();

$content = <<<EOF
<div class="footer-logo links"><img src="{{skin url="images/logo.png"}}"/>
<div class="address">
Birkhøjen 6D, 8382 Hinnerup </br>
T: + 45 11 22 33 44 - M: info@clkweb.dk
</div>
<div>
<h6>Vi er sociale</h6>
<ul>
<li><a href="#"><img alt="clkwebtheme facebook" src="{{media url="wysiwyg/clkwebtheme_images/social/facebook.png"}}" /></a></li>
<li><a href="#"><img alt="clkwebtheme twitter" src="{{media url="wysiwyg/clkwebtheme_images/social/twitter.png"}}" /></a></li>
<li><a href="#"><img alt="clkwebtheme pinterest" src="{{media url="wysiwyg/clkwebtheme_images/social/pinterest.png"}}" /></a></li>
<li><a href="#"><img alt="clkwebtheme linkedin" src="{{media url="wysiwyg/clkwebtheme_images/social/linkedin.png"}}" /></a></li>
<li><a href="#"><img alt="clkwebtheme social" src="{{media url="wysiwyg/clkwebtheme_images/social/social-1.png"}}" /></a></li>
<li><a href="#"><img alt="clkwebtheme social" src="{{media url="wysiwyg/clkwebtheme_images/social/social-2.png"}}" /></a></li>
<li><a href="#"><img alt="clkwebtheme social" src="{{media url="wysiwyg/clkwebtheme_images/social/social-3.png"}}" /></a></li>
<li><a href="#"><img alt="clkwebtheme social" src="{{media url="wysiwyg/clkwebtheme_images/social/social-4.png"}}" /></a></li>
</ul>
</div>
</div>
EOF;

$installer->addStaticBlock('footer-logo', 'Footer Logo Block', $content);


// Add static block - Credit Card Logo
Mage::getModel('cms/block')->load('credit-card-logo')->delete();

$content = <<<EOF
<p><img alt="credit card logo" src="{{media url="wysiwyg/clkwebtheme_images/credit-logos-new.png"}}" /></p>
EOF;

$installer->addStaticBlock('credit-card-logo', 'Credit Card Logo', $content);


// Add static block - Header Top Left Links
Mage::getModel('cms/block')->load('top-left-links')->delete();

$content = <<<EOF
<div class="header-cms-block header-cms-block-1">
<img src="{{media url="wysiwyg/clkwebtheme_images/clock.png"}}" alt="Levering 1-3 dage" />
<span>Levering 1-3 dage</span>
</div>
<div class="header-cms-block header-cms-block-2">
<img src="{{media url="wysiwyg/clkwebtheme_images/truck.png"}}" alt=" Fri fragt ved køb for 800 kr" />
<span> Fri fragt ved køb for 800 kr.</span>
</div>
EOF;

$installer->addStaticBlock('top-left-links', 'Header Top Left Links', $content);


// Add static block - Header Contact Info
Mage::getModel('cms/block')->load('header-contact-info')->delete();

$content = <<<EOF
<div><span class="header-icon icon-phone">&nbsp;</span>+45 12 34 56 78</div>
<div><span class="header-icon icon-mail">&nbsp;</span><a href="mailto:info@greentheme.dk">info@greentheme.dk</a></div>
EOF;

$installer->addStaticBlock('header-contact-info', 'Header Contact Info', $content);


// Add static block - Home Promo Blocks
Mage::getModel('cms/block')->load('home-promo-blocks')->delete();

$content = <<<EOF
<div class="home-promo first"><img src="{{media url="wysiwyg/clkwebtheme_images/banner-left.png"}}" alt="" /></div>
<div class="home-promo last product-cms-block">{{block type="cms/block" block_id="free_shipping_300kr"}}</div>
EOF;

$installer->addStaticBlock('home-promo-blocks', 'Home Promo Blocks', $content);


// Add static block - Home Text Title
Mage::getModel('cms/block')->load('home_text_title')->delete();

$content = <<<EOF
GreenTheme - Lorem ipsum dolor sit amet, consectetur adipiscing elit!
EOF;

$installer->addStaticBlock('home_text_title', 'Home Text Title', $content);


// Add static block - Homepage Text - 1
Mage::getModel('cms/block')->load('home_text_1')->delete();

$content = <<<EOF
<h5>Lorem Ipsum</h5>
<p>Suspendisse eu diam in mauris ullamcorper dapibus. Nulla dignissim sem vel felis blandit semper. Curabitur tristique accumsan massa sit amet viverra. Vestibulum condimentum nisl metus, et efficitur tellus laoreet eget. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed eu posuere lacus.</p>
<h5>Dolor Sit Amet</h5>
<p>Cras eget elit at lacus euismod pretium. Nam tristique facilisis eros a <a href="#"> iaculis</a>. Cras a metus aliquam, finibus nulla et, convallis arcu. Vivamus elementum diam sit amet dui maximus posuere. Etiam bibendum pulvinar enim ut condimentum. </p>
EOF;

$installer->addStaticBlock('home_text_1', 'Homepage Text - 1', $content);

// Add static block - Homepage Text - 2
Mage::getModel('cms/block')->load('home_text_2')->delete();

$content = <<<EOF
<h5>Lorem Ipsum</h5>
<p>Suspendisse eu diam in mauris ullamcorper dapibus. Nulla dignissim sem vel felis blandit semper. Curabitur tristique accumsan massa sit amet viverra. Vestibulum condimentum nisl metus, et efficitur tellus laoreet eget. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed eu posuere lacus.</p>
<h5>Dolor Sit Amet</h5>
<p>Cras eget elit at lacus euismod pretium. Nam tristique facilisis eros a <a href="#"> iaculis</a>. Cras a metus aliquam, finibus nulla et, convallis arcu. Vivamus elementum diam sit amet dui maximus posuere. Etiam bibendum pulvinar enim ut condimentum. </p>
EOF;

$installer->addStaticBlock('home_text_2', 'Homepage Text - 2', $content);

// Add static block - Homepage Text - 3
Mage::getModel('cms/block')->load('home_text_3')->delete();

$content = <<<EOF
<h5>Lorem Ipsum</h5>
<p>Suspendisse eu diam in mauris ullamcorper dapibus. Nulla dignissim sem vel felis blandit semper. Curabitur tristique accumsan massa sit amet viverra. Vestibulum condimentum nisl metus, et efficitur tellus laoreet eget. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed eu posuere lacus.</p>
<h5>Dolor Sit Amet</h5>
<p>Cras eget elit at lacus euismod pretium. Nam tristique facilisis eros a <a href="#"> iaculis</a>. Cras a metus aliquam, finibus nulla et, convallis arcu. Vivamus elementum diam sit amet dui maximus posuere. Etiam bibendum pulvinar enim ut condimentum. </p>
EOF;

$installer->addStaticBlock('home_text_3', 'Homepage Text - 3', $content);

// Add static block - Size Guide
Mage::getModel('cms/block')->load('size_guide')->delete();

$content = <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
EOF;

$installer->addStaticBlock('size_guide', 'Size Guide', $content);


// Add static block - Product - Free shipping from 300 kr
Mage::getModel('cms/block')->load('free_shipping_300kr')->delete();

$content = <<<EOF
<h3><img src="{{media url="wysiwyg/clkwebtheme_images/icon-shipping.png"}}" alt="GRATIS FRAGT" /><strong>GRATIS FRAGT</strong> VED K&Oslash;B FOR 300 KR</h3>

<ul>
<li>30 DAGES FULD RETURRET</li>
<li>PRISGARANTI</li>
</ul>

<ul>
<li>1-3 DAGES LEVERING</li>
<li>GRATIS RETURNERING</li>
</ul>

<ul>
<li>SIKKER BETALING</li>
<li>TELEFONISK SUPPORT</li>
</ul>
EOF;

$installer->addStaticBlock('free_shipping_300kr', 'Product - Free shipping from 300 kr', $content);


// Add static block - Product - Same day delivery
Mage::getModel('cms/block')->load('product-promo-delivery')->delete();

$content = <<<EOF
<h3><img src="{{media url="wysiwyg/clkwebtheme_images/icon-shipping.png"}}" alt="GRATIS FRAGT" />Så sender vi din pakke <strong>idag!</strong></h3>

<ul>
<li>30 DAGES FULD RETURRET</li>
<li>PRISGARANTI</li>
</ul>

<ul>
<li>1-3 DAGES LEVERING</li>
<li>GRATIS RETURNERING</li>
</ul>

<ul>
<li>SIKKER BETALING</li>
<li>TELEFONISK SUPPORT</li>
</ul>
EOF;

$installer->addStaticBlock('product-promo-delivery', 'Product - Same day delivery', $content);


// Add static block - Contact Us Page Block
Mage::getModel('cms/block')->load('contact-block')->delete();

$content = <<<EOF
<ul>
<li><strong>GreenTheme</strong></li>
<li>Birkhøjen 6D</li>
<li>8382 Hinnerup</li>
</ul>
<ul>
<li>Email: info@greentheme.dk</li>
<li>Telefon: +45 11 22 33 44</li>
</ul>
<ul>
<li><strong>Åbningstid: </strong></li>
<li>Mandag - Fredag 08.00 - 16.00</li>
<li>Lørdag: 08.00 - 15.00 </li>
<li>Søndag: Lukket</li>
</ul>
<img src="{{media url="wysiwyg/clkwebtheme_images/3881831620X400.jpg"}}" alt="contacts-image" />
EOF;

$installer->addStaticBlock('contact-block', 'Contact Us Page Block', $content);


// Add static block - Code For CMS Block Images
Mage::getModel('cms/block')->load('cms-images-block')->delete();

$content = <<<EOF
/// 1 billed fuld bredde

<div class="all-full-width">
<img src="{{media url="wysiwyg/clkwebtheme_images/404-banner-3.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/404-banner-3.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/404-banner-3.jpg"}}" alt="" />

</div>


/// 2 billed fuld bredde

<div class="two-in-row">
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />

</div>


/// 3 billed fuld bredde

<div class="three-in-row">

<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />

</div>

/// 4 billed fuld bredde

<div class="four-in-row">
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />

</div>

/// 5 billed fuld bredde

<div class="five-in-row">

<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />

</div>

// 1/3 and 2/3 together
<div class="one-third-first">
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
</div>

// 2/3 and 1/3 together
<div class="two-third-first">
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
<img src="{{media url="wysiwyg/clkwebtheme_images/about_us.jpg"}}" alt="" />
</div>
EOF;

$installer->addStaticBlock('cms-images-block', 'Code For CMS Block Images', $content);


$installer->endSetup();