<?php
class Clkweb_Theme_Model_Observer
{
    // set clkweb_theme.xml before local.xml
    public function addLayoutXml($event)
    {
        $xml = $event->getUpdates()->addChild('clkweb_theme');
        /* @var $xml SimpleXMLElement */
        $xml->addAttribute('module', 'Clkweb_Theme');
        $xml->addChild('file', 'clkweb_theme.xml');

        $xml = $event->getUpdates()->addChild('clkweb_theme_boxed');
        /* @var $xml SimpleXMLElement */
        $xml->addAttribute('module', 'Clkweb_Theme');
        $xml->addChild('file', 'clkweb_theme_boxed.xml');

        $xml = $event->getUpdates()->addChild('clkweb_theme_boxedbg');
        /* @var $xml SimpleXMLElement */
        $xml->addAttribute('module', 'Clkweb_Theme');
        $xml->addChild('file', 'clkweb_theme_boxedbg.xml');
    }
}
