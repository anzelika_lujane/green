<?php

class Clkweb_Theme_Model_Shopby_Source_Category_Start extends Amasty_Shopby_Model_Source_Category_Start
{
    const START_ROOT = 0;
    const START_CURRENT = 1;
    const START_CHILDREN = 2;
    const START_PARENT = 3;

    public function toOptionArray()
    {
        $hlp = Mage::helper('amshopby');
        return array(
            array('value' => self::START_ROOT,      'label' => $hlp->__('Root Category')),
            array('value' => self::START_CURRENT,   'label' => $hlp->__('Same As Current Category')),
            array('value' => self::START_CHILDREN,  'label' => $hlp->__('Current Category Children')),
            array('value' => self::START_PARENT,  'label' => $hlp->__('Current Category Level 1 Parent'))
        );
    }
}