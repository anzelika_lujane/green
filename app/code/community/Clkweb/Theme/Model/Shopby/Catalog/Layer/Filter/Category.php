<?php
if (Mage::getConfig()->getModuleConfig('Amasty_Shopby')->is('active', 'true')) {
    class Clkweb_Theme_Model_Shopby_Catalog_Layer_Filter_Category extends Amasty_Shopby_Model_Catalog_Layer_Filter_Category
    {
        public function getAdvancedCollection()
        {
            if (is_null($this->_advancedCollection)) {
                $helper = Mage::helper('amshopby');
                $category = $helper->getCurrentCategory();

                $startFrom = Mage::getStoreConfig('amshopby/advanced_categories/start_category');
                switch ($startFrom) {
                    case Amasty_Shopby_Model_Source_Category_Start::START_CHILDREN:
                        break;
                    case Amasty_Shopby_Model_Source_Category_Start::START_CURRENT:
                        $parent = $category->getParentCategory();
                        if ($parent) {
                            $category = $parent;
                        }
                        break;
                    case Clkweb_Theme_Model_Shopby_Source_Category_Start::START_PARENT:
                        $path = $category->getPath();
                        $ids = explode('/', $path);
                        if (isset($ids[2])){
                            $topParent = $ids[2];
                        } else {
                            $topParent = Mage::app()->getStore()->getRootCategoryId();
                        }
                        $category = Mage::getModel('catalog/category')->load($topParent);
                        break;
                    case Amasty_Shopby_Model_Source_Category_Start::START_ROOT:
                    default:
                        $category = Mage::getModel('catalog/category')->load(Mage::app()->getStore()->getRootCategoryId());
                }

                $cats = $this->_getCategoryCollection()->addIdFilter($category->getChildren());
                $this->addCounts($cats);

                foreach ($cats as $c) {
                    if ($c->getProductCount()) {
                        $this->_advancedCollection = $cats;
                        return $this->_advancedCollection;
                    }
                }

                $this->_advancedCollection = array();
            }

            return $this->_advancedCollection;
        }
    }
} else {
    class Clkweb_Theme_Model_Shopby_Catalog_Layer_Filter_Category extends Mage_Catalog_Model_Layer_Filter_Category
    {
    }
}

