<?php
/**
 * You can put function used in migration scripts here
 *
 */
class Clkweb_Theme_Model_Resource_Setup
    extends Mage_Catalog_Model_Resource_Setup
{
    protected $_ioObject = null;
    protected $_destinationMediaDirectory = null;

    /**
     * Creates the IO object and optionally creates the directory.
     *
     * @return Varien_Io_File
     */
    protected function _getIoObject()
    {
        if ($this->_ioObject === null) {
            $this->_ioObject = new Varien_Io_File();
            $destDirectory = $this->_destinationMediaDirectory;
            try {

                $this->_ioObject->open(array('path' => $destDirectory));
            } catch (Exception $e) {

                    $this->_ioObject->mkdir($destDirectory, 0777, true);
                    $this->_ioObject->open(array('path' => $destDirectory));
            }
        }
        return $this->_ioObject;
    }

    /**
     * Gets the directory from which media files are copied.
     * @param string $package
     * @param string $skin
     */
    protected function _getSourceMediaDirectory($package, $theme)
    {
        $themePath = 'base' . DS . 'default';
        if ($package && $theme) {
            $themePath = $package . DS . $theme;
        }
        return Mage::getBaseDir('skin') . DS .
            'frontend'     . DS .
            $themePath     . DS .
            'images'       . DS .
            'temp_media'   . DS;
    }

    /**
     * Gets the directory in which media files are copied to.
     *
     * @param string $folderPath
     * @return string
     */
    protected function _getDestinationMediaDirectory($folderPath = 'wysiwyg')
    {
        if (!$folderPath) {
            return Mage::getBaseDir('media') . DS;
        }
        return Mage::getBaseDir('media') . DS . $folderPath . DS;
    }

    /**
     * Copies an array of files from a source to a destination media directory.
     *
     * @param string $folderPath
     * @param array $files
     * @return Scandi_Migrations_Model_Resource_Setup
     */
    public function copyMediaFiles($files, $package, $theme, $folderPath = null)
    {
        $sourceDirectory = $this->_getSourceMediaDirectory($package, $theme);
        $destDirectory   = $this->_getDestinationMediaDirectory($folderPath);
        $this->_destinationMediaDirectory = $destDirectory;
        foreach ($files as $file) {
            $sourceFile = $sourceDirectory . $file;
            if ($this->_getIoObject()->fileExists($sourceFile)) {
                $this->_getIoObject()->cp($sourceFile, $destDirectory . $file);
            }
        }
        return $this;
    }

    /**
     * Add or update existing static block
     *
     * @param string $identifier
     * @param string $name
     * @param string $content
     */
    public function addStaticBlock($identifier, $title, $content, $customerLoc = array(-1), $stores = array(0))
    {
        Mage::getModel('cms/block')
            ->load($identifier, 'identifier')
            ->setIdentifier($identifier)
            ->setContent($content)
            ->setTitle($title)
            ->setStores($stores)
            ->setCustomerLocation($customerLoc)
            ->save();
    }

    /**
     * Add or update existing cms page
     *
     * @param string $identifier
     * @param string $name
     * @param string $content
     */
    public function addCmsPage($identifier, $title, $heading, $content, $template, $layout = '', $stores = array(0))
    {
        Mage::getModel('cms/page')
            ->load($identifier, 'identifier')
            ->setIdentifier($identifier)
            ->setContentHeading($heading)
            ->setContent($content)
            ->setTitle($title)
            ->setStores($stores)
            ->setRootTemplate($template)
            ->setLayoutUpdateXml($layout)
            ->save();
    }

    /**
     * Easily recursively create categories. $parent is category you want
     * to attach your upper newly created categories. $children is an array
     * of category names that you wan't to creat, you can make subarrays to
     * create subcategories
     *
     * @param Mage_Catalog_Model_Category $parent
     * @param array $children
     * @return Drecomm_Migrations_Model_Resource_Setup
     */
    public function createCategories($parent, $children)
    {
        foreach ($children as $key => $child) {
            $cat = Mage::getModel('catalog/category');
            if (is_array($child)) {
                $cat->setName($key)
                    ->setIsActive(true)
                    ->setParent($parent->getId())
                    ->setPath($parent->getPath())
                    ->setAttributeSetId($cat->getDefaultAttributeSetId())
                    ->save();

                $this->createCategories($cat, $child);
            } else {
                $cat->setName($child)
                    ->setIsActive(true)
                    ->setParent($parent->getId())
                    ->setPath($parent->getPath())
                    ->setAttributeSetId($cat->getDefaultAttributeSetId())
                    ->save();
            }
        }
        return $this;
    }

    /**
     * Function allows to create attributes passing just an array with attribute data.
     * @param array $data
     */
    public function attributeQuickCreate($data)
    {
        foreach ($data as $key => $value) {
            $this->removeAttribute('catalog_product', $key);
            $this->addAttribute('catalog_product', $key, array(
                'group'                    => $value['group'],
                'label'                    => $value['label'],
                'input'                    => $value['input'],
                'type'                     => $value['type'],
                'user_defined'             => true,
                'required'                 => false,
                'apply_to'                 => 'simple,configurable,grouped,bundle',
                'is_filterable'            => '0',
                'filterable_in_search'     => '0',
                'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'default'                  => false,
                'is_html_allowed_on_front' => true,
                'wysiwyg_enabled'          => true
            ));
        }
    }
}