jQuery(document).ready(function() {

    jQuery('.page .header-language-background .links ul li.has-dropdown').on('click', function() {
        jQuery('.page .header-language-background .links ul li.has-dropdown > ul').toggle();
    });

    jQuery('.header-minicart').on('mouseenter', function() {
        jQuery(this).find('.skip-cart, .skip-content').addClass('skip-active');
    });

    jQuery('.header-minicart').on('mouseleave', function() {
        jQuery(this).find('.skip-cart, .skip-content').removeClass('skip-active');
    });

    initHomeCategoryTabs();
    initBrandSlider();
    resizeBrands();
    forceCartLink();
});

jQuery(window).on('delayed-resize', function (e, resizeEvent) {
    resizeBrands();
});
jQuery(window).load(function() {
    resizeBrands();
    if (jQuery('.products-grid').length) {
        alignProductGridName();
        alignProductGridActions();

        // Since the height of each cell and the number of columns per page may change when the page is resized, we are
        // going to run the alignment function each time the page is resized.
        jQuery(window).on('delayed-resize', function (e, resizeEvent) {
            alignProductGridName();
        });
    }
});

var initHomeCategoryTabs = function() {
    var home = jQuery('.cms-index-index');
    home.find('.category-products').first().show();
    home.find('.category-products .title-container').first().addClass('active');
    home.find('.homepage-category-titles').html(home.find('.category-products .title-container'));
    home.find('.title-container').on('click', function() {
        home.find('.title-container').removeClass('active');
        jQuery(this).addClass('active');
        home.find('.category-products').hide();
        home.find('#content-' + jQuery(this).attr('id')).show();
        alignProductGridName();
        alignProductGridActions();
    });
};

var initPopup = function(btnActivate, popupContainer) {
    var popup = jQuery(popupContainer);
    var btnActivate = jQuery(btnActivate);
    var btnClose = popup.find('.close');
    var overlay = popup.next('.popup-overlay');
    popup.hide();
    overlay.hide();
    btnActivate.on('click', function() {
        popup.fadeIn(300);
        overlay.show();
    });
    btnClose.on('click', function() {
        popup.hide();
        overlay.hide();
    });
};

var alignProductGridName = function () {
    // Loop through each product grid on the page
    jQuery('.products-grid').each(function(){
        var gridRows = []; // This will store an array per row
        var tempRow = [];
        productGridElements = jQuery(this).children('li');
        productGridElements.each(function (index) {
            // The JS ought to be agnostic of the specific CSS breakpoints, so we are dynamically checking to find
            // each row by grouping all cells (eg, li elements) up until we find an element that is cleared.
            // We are ignoring the first cell since it will always be cleared.
            if (jQuery(this).css('clear') != 'none' && index != 0) {
                gridRows.push(tempRow); // Add the previous set of rows to the main array
                tempRow = []; // Reset the array since we're on a new row
            }
            tempRow.push(this);

            // The last row will not contain any cells that clear that row, so we check to see if this is the last cell
            // in the grid, and if so, we add its row to the array
            if (productGridElements.length == index + 1) {
                gridRows.push(tempRow);
            }
        });

        jQuery.each(gridRows, function () {
            var tallestProductInfo = 0;
            jQuery.each(this, function () {
                // Since this function is called every time the page is resized, we need to remove the min-height
                // and bottom-padding so each cell can return to its natural size before being measured.
                jQuery(this).find('.product-name').css({
                    'min-height': ''
                });

                // We are checking the height of .product-info (rather than the entire li), because the images
                // will not be loaded when this JS is run.
                var productInfoHeight = jQuery(this).find('.product-name').height();
                // Space above .actions element
                var actionSpacing = 0;
                var additionalSpacing = 2;

                // Add height of two elements. This is necessary since .actions is absolutely positioned and won't
                // be included in the height of .product-info
                var totalHeight = productInfoHeight + actionSpacing + additionalSpacing;
                if (totalHeight > tallestProductInfo) {
                    tallestProductInfo = totalHeight;
                }
            });
            // Set the height of all .product-info elements in a row to the tallest height
            jQuery.each(this, function () {
                jQuery(this).find('.product-name').css('min-height', tallestProductInfo);
            });
        });
    });
};

var alignProductGridActions = function () {
    // Loop through each product grid on the page
    $j('.products-grid').each(function(){
        var gridRows = []; // This will store an array per row
        var tempRow = [];
        productGridElements = $j(this).children('li');
        productGridElements.each(function (index) {
            // The JS ought to be agnostic of the specific CSS breakpoints, so we are dynamically checking to find
            // each row by grouping all cells (eg, li elements) up until we find an element that is cleared.
            // We are ignoring the first cell since it will always be cleared.
            if ($j(this).css('clear') != 'none' && index != 0) {
                gridRows.push(tempRow); // Add the previous set of rows to the main array
                tempRow = []; // Reset the array since we're on a new row
            }
            tempRow.push(this);

            // The last row will not contain any cells that clear that row, so we check to see if this is the last cell
            // in the grid, and if so, we add its row to the array
            if (productGridElements.length == index + 1) {
                gridRows.push(tempRow);
            }
        });

        $j.each(gridRows, function () {
            var tallestProductInfo = 0;
            $j.each(this, function () {
                // Since this function is called every time the page is resized, we need to remove the min-height
                // and bottom-padding so each cell can return to its natural size before being measured.
                $j(this).find('.product-info').css({
                    'min-height': '',
                    'padding-bottom': ''
                });

                // We are checking the height of .product-info (rather than the entire li), because the images
                // will not be loaded when this JS is run.
                var productInfoHeight = $j(this).find('.product-info').height();
                // Space above .actions element
                var actionSpacing = 10;
                // The height of the absolutely positioned .actions element
                var actionHeight = $j(this).find('.product-info .actions').height();

                // Add height of two elements. This is necessary since .actions is absolutely positioned and won't
                // be included in the height of .product-info
                var totalHeight = productInfoHeight + actionSpacing + actionHeight;
                if (totalHeight > tallestProductInfo) {
                    tallestProductInfo = totalHeight;
                }

                // Set the bottom-padding to accommodate the height of the .actions element. Note: if .actions
                // elements are of varying heights, they will not be aligned.
                if (actionHeight != 0) {
                    $j(this).find('.product-info').css('padding-bottom', actionHeight + 'px');
                }
            });
            // Set the height of all .product-info elements in a row to the tallest height
            $j.each(this, function () {
                if (tallestProductInfo != 0) {
                    $j(this).find('.product-info').css('min-height', tallestProductInfo);
                }
            });
        });
    });
};

Minicart.prototype.updateContentOnRemove = function(result, el) {
    var cart = this;
    el.hide('slow', function() {
        $j(cart.selectors.container).html(result.content);
        cart.showMessage(result);
        if (result.qty == 0) {
            $j('div.header-minicart').find('.subtotal').html(minicartZeroValue);
        } else {
            $j('div.header-minicart')
                .find('.subtotal')
                .html($j(cart.selectors.container).find('.subtotal .price').html());
        }
    });
};

Minicart.prototype.updateContentOnUpdate = function(result) {
    $j(this.selectors.container).html(result.content);
    this.showMessage(result);
    $j('div.header-minicart')
        .find('.subtotal')
        .html($j(this.selectors.container).find('.subtotal .price').html());
};

var initBrandSlider = function() {
    jQuery('.brand-slider').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        useCSS: false,
        autoplay: true,
        arrows : false,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 770,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 560,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 470,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
};
var resizeBrands = function() {
    var item = jQuery('.brands-page').find('li');
    var ratio = 1.54;
    var height = parseInt(item.outerWidth() / ratio);
    var inHeight = parseInt(item.width() / ratio);
    item.css('height', height);

    item.find('img').each(function(idx, el) {
        var parentHeight = jQuery(el).parents('li').height();
        if (jQuery(el).outerHeight() < parentHeight && jQuery(el).outerHeight() < jQuery(el).outerWidth()) {
            jQuery(el).css('margin-top', ((parentHeight - jQuery(el).outerHeight()) / 2));
        } else {
            jQuery(el).css('margin-top', 0);
        }
    });
};

var forceCartLink = function() {
    jQuery('.page-header .header-minicart').on('click', function() {
        window.location.href = jQuery('.top-link-cart').attr('href');
    });
};
