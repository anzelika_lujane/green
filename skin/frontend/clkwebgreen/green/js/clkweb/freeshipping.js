function component(x, v) {
    return Math.floor(x / v);
}

document.observe("dom:loaded", function() {
    setInterval(function() {

        timestamp--;

        var hours   = component(timestamp,      60 * 60) % 24,
            minutes = component(timestamp,           60) % 60,
            seconds = component(timestamp,            1) % 60;
        var text = Translator.translate('Make an order now and it will be shipped today! Offer ends in %h hours %m minutes and %s seconds!');
        
        text = text.replace('%h', (hours < 10 ? '0' + hours : hours));
        text = text.replace('%m', (minutes < 10 ? '0' + minutes : minutes));
        text = text.replace('%s', (seconds < 10 ? '0' + seconds : seconds));
        
        $$('.delivery-offer-message span')[0].update(text);

    }, 1000);
});